package com.atlassian.bamboo.plugins.confdeploy;

import org.apache.http.HttpEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.message.BasicNameValuePair;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class Product
{
    public static final Product CONFLUENCE = new Product("doauthenticate.action?os_authType=basic", "password");
    public static final Product JIRA = new Product("secure/admin/WebSudoAuthenticate.jspa?os_authType=basic", "webSudoPassword");
    public static final Product BAMBOO = new ProductNoSudo("userlogin.action?os_authType=basic", "os_username", "os_password");
    public static final Product FECRU = new ProductNoSudo("login?os_authType=basic", "username", "password");

    public static class ProductNoSudo extends Product
    {
        private final String usernameFieldName;

        ProductNoSudo(String webSudoAuthenticateRelativeURL, String usernameFieldName, String passwordFieldName)
        {
            super(webSudoAuthenticateRelativeURL, passwordFieldName);
            this.usernameFieldName = usernameFieldName;
        }

        @Override
        protected List<BasicNameValuePair> getAuthenticationParams(String username, String password)
        {
            List<BasicNameValuePair> values = super.getAuthenticationParams(username, password);
            values.add(new BasicNameValuePair(usernameFieldName, username));
            return values;
        };

        public boolean hasWebSudo()
        {
            return false;
        };
    };

    private static final String ENCODING = "utf-8";

    private final String webSudoAuthenticateRelativeURL;
    private final String passwordFieldName;

    Product(String webSudoAuthenticateRelativeURL, String passwordFieldName)
    {
        this.webSudoAuthenticateRelativeURL = webSudoAuthenticateRelativeURL;
        this.passwordFieldName = passwordFieldName;
    }

    public String getWebSudoAuthenticationURL(String baseURL)
    {
        if (baseURL.endsWith("/"))
            return baseURL + webSudoAuthenticateRelativeURL;

        return baseURL + "/" + webSudoAuthenticateRelativeURL;
    }

    public HttpEntity getPluginUploadEntity(File plugin)
    {
        FileBody uploadFile = new FileBody(plugin);
        MultipartEntity uploadEntity = new MultipartEntity();
        uploadEntity.addPart("plugin", uploadFile);

        return uploadEntity;
    }

    public HttpEntity getWebSudoAuthenticationEntity(String username, String password) throws UnsupportedEncodingException
    {
        return new UrlEncodedFormEntity(getAuthenticationParams(username, password), ENCODING);
    }

    protected List<BasicNameValuePair> getAuthenticationParams(String username, String password)
    {
        List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
        values.add(new BasicNameValuePair(passwordFieldName, password));
        values.add(new BasicNameValuePair("authenticate", "Confirm"));
        return values;
    }

    public boolean hasWebSudo()
    {
        return true;
    }
}
