package com.atlassian.bamboo.plugins.confdeploy;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.plan.artifact.ArtifactSubscriptionContext;
import com.atlassian.bamboo.plan.artifact.ArtifactSubscriptionManager;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;
import org.jetbrains.annotations.NotNull;

import java.io.File;

public abstract class AutoDeployTask implements TaskType
{
    public static final String PLUGIN_ARTIFACT = "confDeployJar";
    public static final String CONFLUENCE_BASE_URL = "confDeployURL";
    public static final String CONFLUENCE_ADMIN_USER = "confDeployUsername";
    public static final String CONFLUENCE_ADMIN_PASSWORD = "confDeployPassword";
    public static final String CONFLUENCE_KEY = "confDeployKey";

    private static final String ENDPOINT_RELATIVE_URL = "rpc/soap-axis/confluenceservice-v2";

    public abstract Product getTargetProduct();

    private static String urlConcat(String base, String relative)
    {
        if (base.endsWith("/"))
        {
            return base + relative;
        }
        return base + "/" + relative;
    }

    @NotNull
    @java.lang.Override
    public TaskResult execute(@NotNull final TaskContext taskContext) throws TaskException
    {
        final BuildLogger buildLogger = taskContext.getBuildLogger();

        // Extract the required parameters
        final String confluenceBaseURL = taskContext.getConfigurationMap().get(CONFLUENCE_BASE_URL);
        final String actualURL = urlConcat(confluenceBaseURL, ENDPOINT_RELATIVE_URL);

        final String username = taskContext.getConfigurationMap().get(CONFLUENCE_ADMIN_USER);
        final String encryptedPassword = taskContext.getConfigurationMap().get(CONFLUENCE_ADMIN_PASSWORD);
        final String passwordKey = taskContext.getConfigurationMap().get(CONFLUENCE_KEY);
        final String rawPassword = Crypto.decrypt(passwordKey, encryptedPassword);

        // Retrieve the referenced artifact definition
        final String artifactKey = taskContext.getConfigurationMap().get(PLUGIN_ARTIFACT);

        // TODO: Retrieve the artifact subscription by ID
        //final ArtifactSubscription pluginArtifact = artifactSubscriptionManager.findSubscription(Long.valueOf(artifactKey));

        // Ensure that the artifact exists and is accessible.
        File pluginJar = null;
        for (ArtifactSubscriptionContext context : taskContext.getBuildContext().getArtifactContext().getSubscriptionContexts())
        {
            if (context.getArtifactDefinitionContext().getName().equals(artifactKey))
            {
                pluginJar = new File(context.getEffectiveDestinationPath());
                break;
            }
        }
        if (pluginJar == null)
        {
            final String msg = String.format("Unable to find the artifact with id %s in this task's context", artifactKey);
            buildLogger.addErrorLogEntry(msg);
            throw new TaskException(msg);
        }

        if (!pluginJar.exists() || !pluginJar.isFile())
        {
            final String msg = String.format("Artifact file %s is either missing or not a file.", pluginJar.getAbsolutePath());
            buildLogger.addErrorLogEntry(msg);
            throw new TaskException(msg);
        }

        UniversalPluginUploader uploader = new UniversalPluginUploader(buildLogger);
        uploader.upload(getTargetProduct(), pluginJar, confluenceBaseURL, username, rawPassword);


        return TaskResultBuilder.create(taskContext).success().build();
    }
}