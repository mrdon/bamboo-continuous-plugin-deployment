package com.atlassian.bamboo.plugins.confdeploy;

import com.atlassian.bamboo.plan.artifact.ArtifactSubscriptionManager;
import com.atlassian.sal.api.component.ComponentLocator;

/**
 *
 */
public class ServiceAccessor
{
    public static ArtifactSubscriptionManager getArtifactSubscriptionManager()
    {
        return ComponentLocator.getComponent(ArtifactSubscriptionManager.class);
    }
}
